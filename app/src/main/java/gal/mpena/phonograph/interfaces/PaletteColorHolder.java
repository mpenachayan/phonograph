package gal.mpena.phonograph.interfaces;

import androidx.annotation.ColorInt;

/**
 * @author Aidan Follestad (afollestad)
 */
public interface PaletteColorHolder {

    @ColorInt
    int getPaletteColor();
}
